#!/usr/bin/env bash

echo -e "\\n\\ninstalling to node"
echo "=============================="

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2s/install.sh | bash

nvm install --lts

mkdir ~/code
