#!/usr/bin/env bash

DOTFILES=$HOME/.dotfiles

get_linkables() {
    find -H "$DOTFILES" -maxdepth 3 -name '*.symlink'
}

echo -e "\\nCreating symlinks"
echo "=============================="

for file in $(get_linkables); do
    target="$HOME/.$(basename "$file" '.symlink')"
    if [ -e "$target" ]; then
        echo "~${target#$HOME} already exists... Skipping."
    else
        echo "Creating symlink for $file"
        ln -s "$file" "$target"
    fi
done
