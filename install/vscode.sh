#!/usr/bin/env bash

DOTFILES=$HOME/.dotfiles

echo -e "\\n\\ninstalling to vscode"
echo "=============================="

extension_file="$DOTFILES/vscode/extensions.txt"
cat "$extension_file" | xargs -n 1 code --install-extension

settings="$DOTFILES/vscode/settings.txt"
target=""

if [[ "$(uname)" == "Darwin" ]]; then
    target="$HOME/Library/Application\ Support/Code/User/settings.json"
else
    target="$HOME/.config/Code/User/settings.json"
fi


if [ -e "$target" ]; then
    echo "~${target#$HOME} already exists... Skipping."
    rm -f "$target"
else
    echo "Creating symlink for $settings"
    ln -s "$settings" "$target"
fi
